﻿using System.Diagnostics;
using ADR.Models;

namespace ADR.Services;

public class AdrBuilderService(
    AdrConfig config,
    AdrTemplateWriterService templateWriter,
    AdrIndexService adrIndexService)
{
    private bool _force;
    private bool _launch;
    private string _supersedes = default!;
    private TemplateType _templateType;
    private string _title = "ADR Record";
    private bool _write;

    public AdrBuilderService Title(string title)
    {
        _title = title;
        return this;
    }

    public AdrBuilderService Supersedes(string supersedes)
    {
        _supersedes = supersedes;

        return this;
    }

    public AdrBuilderService Force()
    {
        _force = true;

        return this;
    }

    public AdrBuilderService Write(TemplateType templateType)
    {
        _templateType = templateType;
        _write = true;
        return this;
    }

    public AdrBuilderService LaunchEditor()
    {
        _launch = true;
        return this;
    }

    public void Build()
    {
        if (_supersedes is not null && !_write)
            throw new InvalidOperationException("Must write the ADR before setting the supersedes.");

        if (_launch && !_write)
            throw new InvalidOperationException("Must write the ADR before opening it.");

        if (_write)
        {
            var (title, file) = templateWriter.WriteTemplate(_templateType, _title, _force);

            if (_supersedes is not null)
                templateWriter.UpdateSupersededAdr(_supersedes, title, file);

            adrIndexService.UpdateIndex();

            if (_launch)
            {
                new Process
                {
                    StartInfo = new ProcessStartInfo(Path.Join(config.DocFolder, file))
                    {
                        UseShellExecute = true
                    }
                }.Start();
            }
        }
    }
}
