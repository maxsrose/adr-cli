﻿using System.Text.RegularExpressions;
using ADR.Models;
using Microsoft.Extensions.Logging;

namespace ADR.Services;

public class AdrTemplateWriterService(AdrConfig config, ILogger<AdrTemplateWriterService> logger)
{
    private int GetFileNumber => Directory.Exists(config.DocFolder) ? GetNextFileNumber() : 1;

    public (string title, string fileName) WriteTemplate(TemplateType type, string title, bool force)
    {
        logger.LogDebug("Writing template of type {Type}", type);

        switch (type)
        {
            case TemplateType.Adr:
                return WriteAdr(title, force);

            case TemplateType.New:
                return WriteNew(title, force);

            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    private (string title, string fileName) WriteNew(string title, bool force = false)
    {
        var fileNumber = GetFileNumber;

        var fileName = $"{fileNumber:D4}-{SanitizeTitle(title)}.md";

        var result = Directory.CreateDirectory(config.DocFolder);

        logger.LogDebug("Creating ADR file {FileName} in dir {Directory}", fileName, result.FullName);

        var content = GetAdrTemplate(fileNumber, title);

        WriteFile(result.FullName, fileName, content, force);

        return ($"{fileNumber:D4} - {title}", fileName);
    }

    private (string title, string fileName) WriteAdr(string title, bool force = false)
    {
        var fileNumber = GetFileNumber;

        var fileName = $"{fileNumber:D4}-{SanitizeTitle(title)}.md";

        var result = Directory.CreateDirectory(config.DocFolder);

        logger.LogDebug("Creating initial ADR file {FileName} in dir {Directory}", fileName, result.FullName);

        var content = GetAdrTemplate(
            fileNumber,
            title,
            AdrStatus.Accepted,
            "We need to record the architectural decisions made on this project.",
            "We will use Architecture Decision Records, as described by Michael Nygard in this article: http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions",
            "See Michael Nygard's article, linked above.");

        WriteFile(result.FullName, fileName, content, force);

        return ($"{fileNumber:D4} - {title}", fileName);
    }

    public void UpdateSupersededAdr(string supersededAdr, string newAdrTitle, string newAdrFile)
    {
        var file = Path.Combine(config.DocFolder, supersededAdr);

        if (!File.Exists(file))
            throw new InvalidOperationException($"The ADR file {file} does not exist.");

        var content = File.ReadAllLines(file);

        if (content?.Length > 0)
        {
            //reading file to update the status 
            var lastNonEmptyLineContainsStatus = false;

            for (var i = 0; i < content.Length; i++)
            {

                if (lastNonEmptyLineContainsStatus &&
                    !string.IsNullOrWhiteSpace(content[i]) &&
                    !Regex.IsMatch(content[i], @"##\s+Context", RegexOptions.IgnoreCase))
                {
                    content[i] = $"Superseded - [{newAdrTitle}]({newAdrFile})";
                    break;
                }

                if (!string.IsNullOrWhiteSpace(content[i]))
                    lastNonEmptyLineContainsStatus = Regex.IsMatch(content[i], @"##\s+Status", RegexOptions.IgnoreCase);
            }

            //writing to the file with new status to `superseded`
            using var writer = File.CreateText(file);

            foreach (var line in content)
            {
                writer.WriteLine(line);
            }
        }
    }

    private void WriteFile(string dir, string fileName, string content, bool force = false)
    {
        var filePath = Path.Combine(dir, fileName);

        if (File.Exists(filePath) && !force)
        {
            throw new InvalidOperationException("The file already exists! If you want to overwrite it, delete it first or pass --force.");
        }

        File.WriteAllText(filePath, content);
        logger.LogDebug("Wrote file {FilePath}", filePath);
    }

    private string GetAdrTemplate(
        int fileNumber, string title,
        AdrStatus status = AdrStatus.Proposed,
        string context = "{context}",
        string decision = "{decision}",
        string consequences = "{consequences}") => string.Format(
        """
        # {0:D4} - {1}

        {2:yyyy-MM-dd}

        ## Status

        {3}

        ## Context

        {4}

        ## Decision

        {5}

        ## Consequences

        {6}
        """,
        fileNumber,
        title,
        DateTime.Today,
        status,
        context,
        decision,
        consequences);

    private int GetNextFileNumber()
    {
        return Directory.GetFiles(config.DocFolder, "*.md", SearchOption.TopDirectoryOnly)
            .Select(Path.GetFileName)
            .Select(f => (f ?? "").Split("-").FirstOrDefault())
            .Select(f =>
            {
                var parsed = int.TryParse(f, out var fileNum);

                return (parsed, fileNum);
            })
            .Where(x => x.parsed)
            .Select(x => x.fileNum)
            .DefaultIfEmpty(0)
            .Max(i => i) + 1;
    }

    private static string SanitizeTitle(string title) => title
        .Replace(" ", "-")
        .ToLowerInvariant();
}
