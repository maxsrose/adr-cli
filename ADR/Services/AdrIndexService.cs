﻿using ADR.Models;
using ConsoleTables;
using Microsoft.Extensions.Logging;

namespace ADR.Services;

public class AdrIndexService(AdrConfig config, ILogger<AdrIndexService> logger)
{
    public void UpdateIndex()
    {
        logger.LogDebug("Updating index");
        var table = CreateTable();
        CreateIndexFile(table);
        logger.LogDebug("Updated index with\n{Table}", table.ToString());
    }

    public ConsoleTable GetIndex() => CreateTable(false);

    private void CreateIndexFile(ConsoleTable table)
    {
        var indexFileName = "0000-index.md";
        var indexFile = Path.Combine(config.DocFolder, indexFileName);

        using var writer = File.CreateText(indexFile);

        writer.WriteLine("# Architecture Decision Records\n");

        table
            .Configure(c =>
            {
                // ReSharper disable once AccessToDisposedClosure
                c.OutputTo = writer;
            })
            .Write(Format.MarkDown);
    }

    private ConsoleTable CreateTable(bool markdown = true)
    {
        var files = Directory.EnumerateFiles(config.DocFolder, "*.md", SearchOption.TopDirectoryOnly)
            .Where(f => Path.GetFileName(f) != "0000-index.md")
            .Select(f => new AdrFile(f, config))
            .OrderBy(f => f.Number);

        var table = new ConsoleTable("Number", "Title", "Superseded by");

        foreach (var file in files)
        {
            table.AddRow(
                file.Number,
                markdown ? GetMarkdownTitle(file) : file.Title,
                file.SupersededFilePath is not null ? GetSupersededFile(file, markdown) : "N/A");
        }

        return table;
    }

    private string GetMarkdownTitle(AdrFile file)
    {
        var title = $"[{file.Title}]({file.FileName})";

        return file.SupersededFilePath is not null
            ? $"~~{title}~~"
            : title;
    }

    private string GetSupersededFile(AdrFile file, bool markdown) => markdown
        ? $"[{file.SupersededFileTitle}]({file.SupersededFilePath})"
        : file.Title;
}
