﻿namespace ADR.Models;

public class AdrConfig
{
    internal const string ConfigFileName = "adr.config.json";

    public string DocFolder { get; set; } = "./docs";
    public string TemplateFolder { get; set; } = "./templates";
}
