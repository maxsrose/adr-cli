﻿namespace ADR.Models;

public enum AdrStatus
{
    Proposed,
    Accepted,
    Superseded
}
