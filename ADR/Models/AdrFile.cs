﻿using System.Text.RegularExpressions;
using Markdig;
using Markdig.Syntax;
using Markdig.Syntax.Inlines;

namespace ADR.Models;

public class AdrFile
{
    public AdrFile(string filePath, AdrConfig settings)
    {
        var fileName = Path.GetFileName(filePath);
        FileName = fileName;

        var num = fileName.Split("-")[0];

        if (int.TryParse(num, out var number))
            Number = number;

        var content = File.Exists(filePath) ? File.ReadAllLines(filePath) : null;

        if (!(content?.Length > 0))
            return;

        foreach (var line in content)
        {
            var headerMatch = Regex.Match(line, @"(?<![^\s])#(\s*)[0-9]+(( [-] )|([.]))[\s]*.*");

            if (headerMatch.Success)
            {
                var header = headerMatch.Value;

                var titleRegex = new Regex(@"(?<![^\s])#(\s*)[0-9]+(( [-] )|([.]))[\s]*");
                Title = titleRegex.Replace(header, string.Empty, 1);
            }

            var superMatch = Regex.Match(line, @"(?<=Superseded - ).*(?=\s*)");

            if (!superMatch.Success)
                continue;

            var superSededLine = superMatch.Value;

            var document = Markdown.Parse(superSededLine);

            SupersededFilePath = document.Select(l => l as ParagraphBlock)
                .Select(s => s?.Inline?.FirstChild as LinkInline)
                .Select(s => s?.Url)
                .FirstOrDefault();

            SupersededFileTitle = document.Select(l => l as ParagraphBlock)
                .Select(s => s?.Inline?.FirstChild as LinkInline)
                .Select(s => s?.FirstChild?.ToString())
                .FirstOrDefault();

            break;
        }
    }

    public string Title { get; } = "Unknown";
    public int Number { get; } = 1;
    public string FileName { get; }

    public string? SupersededFileTitle { get; set; }
    public string? SupersededFilePath { get; set; }
}
