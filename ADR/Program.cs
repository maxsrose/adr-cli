﻿using ADR.Commands;
using ADR.Models;
using ADR.Services;
using Cocona;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Index = ADR.Commands.Index;

var builder = CoconaApp.CreateBuilder();

builder.Configuration
    .AddJsonFile("adr.config.json", true)
    .AddEnvironmentVariables();

builder.Services.AddOptions<AdrConfig>().BindConfiguration("");
builder.Services.AddTransient<AdrConfig>(p => p.GetRequiredService<IOptions<AdrConfig>>().Value);

builder.Services.AddTransient<AdrIndexService>();
builder.Services.AddTransient<AdrBuilderService>();
builder.Services.AddTransient<AdrTemplateWriterService>();

builder.Logging.AddDebug();

var app = builder.Build();

app.AddCommand(Init.Name, Init.Run)
    .WithDescription(Init.Description);

app.AddCommand(NewAdr.Name, NewAdr.Run)
    .WithDescription(NewAdr.Description);

app.AddCommand(Index.Name, Index.Run)
    .WithDescription(Index.Description);

app.Run();
