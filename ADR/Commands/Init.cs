﻿using ADR.Models;
using ADR.Services;
using Cocona;

namespace ADR.Commands;

public class Init
{
    public const string Name = "Init";

    public const string Description = "Init the ADR directory with default template";

    public static void Run(
        AdrBuilderService builder,
        [Argument] string? directory,
        [Option("launch-editor", ['e'])] bool launchEditor = true,
        [Option("force", ['f'])] bool force = false)
    {
        builder
            .Title("Record Architecture Decisions")
            .Write(TemplateType.Adr);

        if (force)
            builder.Force();

        if (launchEditor)
            builder.LaunchEditor();

        builder.Build();
    }
}
