﻿using ADR.Models;
using ADR.Services;
using Cocona;

namespace ADR.Commands;

public class NewAdr
{
    public const string Name = "New";
    public const string Description = "Creates a new ADR file.";

    public static void Run(
        AdrBuilderService builder,
        [Argument] string title,
        [Option("supersedes", ['s'])] string? supersedes,
        [Option("force", ['f'])] bool force = false,
        [Option("launch-editor", ['e'])] bool launchEditor = true)
    {
        builder.Title(title)
            .Write(TemplateType.New);

        if (supersedes is not null)
            builder.Supersedes(supersedes);

        if (force)
            builder.Force();

        if (launchEditor)
            builder.LaunchEditor();

        builder.Build();
    }
}
