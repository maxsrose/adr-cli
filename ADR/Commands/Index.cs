﻿using ADR.Models;
using ADR.Services;
using Cocona;

namespace ADR.Commands;

public class Index
{
    public const string Name = "List";

    public const string Description = "Lists all the ADRs in the doc folder.";

    public static void Run(
        AdrIndexService indexService,
        AdrConfig config,
        [Option("update-index", ['u'])] bool updateIndex)
    {
        if (!Directory.Exists(config.DocFolder))
        {
            Console.WriteLine("No ADRs found. Run `adr-cli init` to create an initial ADR.");
        }

        indexService.GetIndex().Write();

        if (updateIndex)
            indexService.UpdateIndex();
    }
}
